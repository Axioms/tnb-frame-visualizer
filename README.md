# TNB Frame Visualizer
Visually demonstrating the appearance of the vectors composing the TNB frame of a 3D curve, and how they change for each point on the curve.
[View Source Code](https://bitbucket.org/Axioms/tnb-frame-visualizer/src)

## Example
![Example](http://i.imgur.com/ApMsIaW.gif)

[(Direct Link (.gif))](http://i.imgur.com/ApMsIaW.gifv)

## Project Reflection
The information given by the TNB frame provides important insight into the geometric structure of the path the curve describes. The TNB frame is given its name by the three vectors that compose it: The tangent (T), the normal (N), and the binormal (B).

![TNB Frame](https://upload.wikimedia.org/wikipedia/commons/thumb/6/6f/Frenet.png/600px-Frenet.png)  

[(Direct Link (.png))](https://upload.wikimedia.org/wikipedia/commons/thumb/6/6f/Frenet.png/600px-Frenet.png)  

The above diagram illustrates the vectors for a curve, along with the _osculating plane_, which T and N span. As can be seen, all three vectors are perpendicular to each other, producing the "frame" like structure.

Determining the three vectors, T, N, and B, for a curve given by r(t), can be broken down into the following steps:

1. **T, the tangent unit vector, is given by r'/|r'|, making it tangent to the curve.**  
2. **N, the normal unit vector, is given by T'/|T'|, making it perpendicular to T (and the curve).**  
3. **B, the binormal unit vector, is given by T x N, making it perpendicular to both.**  

In this demonstration, the function `r(t) = <t * sin(t), t * cos(t), t>` was used, with `t` going from -100 to 100.

## Further Reflection
While the TNB frame gives detailed information about the path of the curve, this strictly geometric description lacks any information about the motion of an object traveling the path. A next step could be to take the parameter t to mean time and include the velocity and acceleration vectors into the visualization. This addition would help demonstrate the kinematic motion of an object traveling through space. The acceleration vector would provide insight into how the direction of the traveling "particle" is changed, while the velocity vector (the direction of which is already shown here by the tangent unit vector) would tell how quickly it is currently moving.


![Example](http://i.imgur.com/mfb4TA0.gif)

[(Direct Link (.gif))](http://i.imgur.com/mfb4TA0.gifv)

## Additional Details
This program was created using the [SFML](http://www.sfml-dev.org/) graphics library. The 3D rendering engine is built off the [perspective projection](https://bitbucket.org/Axioms/perspective-projection/src) project created as a demonstration of a practical application of vectors for [Journal #1](https://bitbucket.org/Axioms/perspective-projection/src).
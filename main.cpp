/**
 * Projecting 3D points onto a 2D plane.
 */

#include <iostream>
#include <cmath>
#include <array>
#include <SFML/Graphics.hpp>

const int SCREEN_WIDTH = 1400;
const int SCREEN_HEIGHT = 1400;
const int NUM_POINTS = 10000;
const int ZOOM = 10; //Multiplication factor for t
const int TNB_SIZE = 20; //Length of TNB frame vectors
const float TNB_SPEED = 0.01; //Incrementation of tnb t value
const float T_MIN = -100; //Minimum t value graphed
const float T_MAX = 100; //Maximum t value drawn
const int AXIS_LENGTH = 2000;
const float SPEED = 5;

struct Vector3 {
    float x;
    float y;
    float z;

    //Construct vector
    Vector3(float x0 = 0, float y0 = 0, float z0 = 0) :
            x(x0),
            y(y0),
            z(z0) {
    }

    //Magnitude of vector
    float magnitude() {
        return sqrt(x * x + y * y + z * z);
    }

    //Dot product with another vector
    float dot(Vector3 other) {
        return x * other.x + y * other.y + z * other.z;
    }

    //Cross product with another vector (<this> x <other>)
    Vector3 cross(const Vector3 other) const {
        return Vector3(y * other.z - z * other.y, -(x * other.z - z * other.x),
                x * other.y - y * other.x);
    }

    //Overload + operator
    friend Vector3 operator+(const Vector3 &v1, const Vector3 &v2);
};

Vector3 operator+(const Vector3 &v1, const Vector3 &v2) {
    return Vector3(v1.x + v2.x, v1.y + v2.y, v1.z + v2.z);
}

std::array<bool, sf::Keyboard::KeyCount> keyStates;

bool isKeyPressed(sf::Keyboard::Key key) {
    return keyStates[static_cast<int>(key)];
}

//Evaluate the vector function at t
//r = <t*sin(t), t*cos(t), t>
Vector3 evaluate(float t) {
    Vector3 result;

    result.x = ZOOM * t * std::sin(t);
    result.y = ZOOM * t * std::cos(t);
    result.z = ZOOM * t;

    return result;
}

//Evaluate the first derivative of the vector function at t
Vector3 evaluate_d(float t) {
    Vector3 result;

    result.x = std::sin(t) + t * std::cos(t);
    result.y = std::cos(t) - t * std::sin(t);
    result.z = 1;

    return result;
}

//Evaluate the second derivative of the vector function at t
Vector3 evaluate_d2(float t) {
    Vector3 result;

    result.x = 2 * std::cos(t) - t * std::sin(t);
    result.y = -2 * std::sin(t) - t * std::cos(t);
    result.z = 0;

    return result;
}

//Evaluate the unit tangent vector function at t
Vector3 evaluate_T(float t) {
    Vector3 result;

    result = evaluate_d(t);

    float magnitude = result.magnitude();
    result.x /= magnitude;
    result.y /= magnitude;
    result.z /= magnitude;

    result.x *= ZOOM * TNB_SIZE;
    result.y *= ZOOM * TNB_SIZE;
    result.z *= ZOOM * TNB_SIZE;

    return result;
}

//Evaluate the principal unit tangent vector function at t
Vector3 evaluate_N(float t) {
    Vector3 result;

    result.x = (2 * std::cos(t) - t * std::sin(t)) / std::sqrt(t * t + 2)
            - (t * (std::sin(t) + t * std::cos(t)))
                    / (std::pow(std::sqrt(t * t + 2), 3));
    result.y = (-2 * std::sin(t) - t * std::cos(t)) / std::sqrt(t * t + 2)
            - (t * (std::cos(t) + t * std::sin(t)))
                    / (std::pow(std::sqrt(t * t + 2), 3));
    result.z = 0;

    float denominator = result.magnitude();
    result.x /= denominator;
    result.y /= denominator;
    result.z /= denominator;

    result.x *= ZOOM * TNB_SIZE;
    result.y *= ZOOM * TNB_SIZE;
    result.z *= ZOOM * TNB_SIZE;

    return result;
}

//Evaluate the binormal vector function at t
Vector3 evaluate_B(float t) {
    Vector3 result;

    Vector3 T = evaluate_T(t);
    Vector3 N = evaluate_N(t);
    result = T.cross(N);

    float magnitude = result.magnitude();
    result.x /= magnitude;
    result.y /= magnitude;
    result.z /= magnitude;

    result.x *= ZOOM * TNB_SIZE;
    result.y *= ZOOM * TNB_SIZE;
    result.z *= ZOOM * TNB_SIZE;

    return result;
}

////Evaluate the velocity vector function at t
//Vector3 evaluate_V(float t) {
//    Vector3 result;
//
//    Vector3 derivative = evaluate_d(t);
//    result.x =
//
//    float magnitude = derivative.magnitude();
//    result.x /= magnitude;
//    result.y /= magnitude;
//    result.z /= magnitude;
//
//    result.x *= ZOOM * TNB_SIZE;
//    result.y *= ZOOM * TNB_SIZE;
//    result.z *= ZOOM * TNB_SIZE;
//
//    return result;
//}

void setPoint(Vector3 &v, Vector3 value) {
    v.x = value.x;
    v.y = value.y;
    v.z = value.z;
}

int main() {
    /////////// Window and points setup ////////////

    //Set-up the window
    sf::VideoMode videoMode(SCREEN_WIDTH, SCREEN_HEIGHT);
    sf::RenderWindow renderWindow(videoMode, "TNB Frame Visualizer");

    keyStates.fill(false);

    //Set-up points
    srand(time(NULL));
    Vector3 points[NUM_POINTS];
    points[0].x = 0;
    points[0].y = 0;
    points[0].z = 0;

    //Multiply by 2 because i is incremented by 2 (there are 2 points per line)
    float tStep = 2.0 * (T_MAX - T_MIN) / NUM_POINTS;
    float t = T_MIN;
    for (int i = 1; i < NUM_POINTS - 1; i += 2) {
        Vector3 v = evaluate(t);
        setPoint(points[i], v);
        setPoint(points[i + 1], v);
        t += tStep;
    }
    //Set first and last point manually: special cases
    setPoint(points[0], evaluate(T_MIN));
    setPoint(points[NUM_POINTS - 1], evaluate(T_MAX));

    //Make axis
    Vector3 axis[6];
    axis[0] = Vector3(-AXIS_LENGTH / 2, 0, 0);
    axis[1] = Vector3(AXIS_LENGTH / 2, 0, 0);
    axis[2] = Vector3(0, -AXIS_LENGTH / 4, 0);
    axis[3] = Vector3(0, AXIS_LENGTH / 4, 0);
    axis[4] = Vector3(0, 0, -AXIS_LENGTH);
    axis[5] = Vector3(0, 0, AXIS_LENGTH);

    Vector3 tnb[6];
    float tnbT = T_MIN; //t value the TNB frame is currently showing

    sf::VertexArray tnbVertices(sf::PrimitiveType::Lines, 6);
    sf::VertexArray axisVertices(sf::PrimitiveType::Lines, 6);
    sf::VertexArray vertices(sf::PrimitiveType::Lines, NUM_POINTS);

    Vector3 screenHorizontal(SCREEN_WIDTH, 0, 0);
    Vector3 screenVertical(0, 0, SCREEN_HEIGHT);
    Vector3 camera(SCREEN_WIDTH / 2, -500, SCREEN_HEIGHT / 2);
    Vector3 camPos(SCREEN_WIDTH / 2, -500, SCREEN_HEIGHT / 2);

    /////////// Main program loop ////////////
    while (renderWindow.isOpen()) {

        /////////// Handle input events ////////////
        sf::Event event;
        while (renderWindow.pollEvent(event)) {
            switch (event.type) {
                case (sf::Event::Closed):
                    renderWindow.close();
                    break;
                case (sf::Event::KeyPressed):
                    keyStates[static_cast<int>(event.key.code)] = true;
                    if (event.key.code == sf::Keyboard::Escape) {
                        renderWindow.close();
                    }
                    break;
                case (sf::Event::KeyReleased):
                    keyStates[static_cast<int>(event.key.code)] = false;
                    break;
                default:
                    break;
            }
        }
        if (isKeyPressed(sf::Keyboard::Left)) {
            camPos.x -= SPEED;
        }
        if (isKeyPressed(sf::Keyboard::Right)) {
            camPos.x += SPEED;
        }
        if (isKeyPressed(sf::Keyboard::Up)) {
            camPos.z -= SPEED;
        }
        if (isKeyPressed(sf::Keyboard::Down)) {
            camPos.z += SPEED;
        }
        if (isKeyPressed(sf::Keyboard::W)) {
            camPos.y += SPEED / 2;
        }
        if (isKeyPressed(sf::Keyboard::S)) {
            camPos.y -= SPEED / 2;
        }

        //Update TNB frame
        if (isKeyPressed(sf::Keyboard::Q)) {
            tnbT -= TNB_SPEED;
        }
        if (isKeyPressed(sf::Keyboard::E)) {
            tnbT += TNB_SPEED;
        }
        if (tnbT > T_MAX) {
            tnbT = T_MIN;
        }
        if (tnbT < T_MIN) {
            tnbT = T_MAX;
        }
        tnb[0] = evaluate(tnbT);
        tnb[1] = tnb[0] + evaluate_T(tnbT);
        tnb[2] = evaluate(tnbT);
        tnb[3] = tnb[2] + evaluate_N(tnbT);
        tnb[4] = evaluate(tnbT);
        tnb[5] = tnb[4] + evaluate_B(tnbT);

        Vector3 normal = screenHorizontal.cross(screenVertical);
        //Update 2D projections of axis
        for (int i = 0; i < axisVertices.getVertexCount(); i++) {
            sf::Vector2f pos;

            float t = (normal.z * screenHorizontal.z - normal.z * camera.z
                    + normal.y * screenHorizontal.y - normal.y * camera.y
                    + normal.x * screenHorizontal.x - normal.x * camera.x)
                    / (normal.x * (axis[i].x - camPos.x)
                            + normal.y * (axis[i].y - camPos.y)
                            + normal.z * (axis[i].z - camPos.z));

            pos.x = camera.x + (axis[i].x - camPos.x) * t;
            pos.y = camera.z + (axis[i].z - camPos.z) * t;

            //Don't draw line if one of its vertices is offscreen
            bool skipNext = false;
            if (axis[i].y < camPos.y) {
                pos.x = 0;
                pos.y = 0;

                if (i % 2 == 0) {
                    axisVertices[i + 1] = sf::Vertex(pos);
                    skipNext = true;
                } else {
                    axisVertices[i - 1] = sf::Vertex(pos);
                }
            }
            axisVertices[i] = sf::Vertex(pos, sf::Color::Magenta);
            if (skipNext) {
                i++;
            }
        }

        //Update 2D projections of points
        for (int i = 0; i < vertices.getVertexCount(); i++) {
            sf::Vector2f pos;

            float t = (normal.z * screenHorizontal.z - normal.z * camera.z
                    + normal.y * screenHorizontal.y - normal.y * camera.y
                    + normal.x * screenHorizontal.x - normal.x * camera.x)
                    / (normal.x * (points[i].x - camPos.x)
                            + normal.y * (points[i].y - camPos.y)
                            + normal.z * (points[i].z - camPos.z));

            pos.x = camera.x + (points[i].x - camPos.x) * t;
            pos.y = camera.z + (points[i].z - camPos.z) * t;

            //Don't draw line if one of its vertices is offscreen
            bool skipNext = false;
            if (points[i].y < camPos.y) {
                pos.x = 0;
                pos.y = 0;

                if (i % 2 == 0) {
                    vertices[i + 1] = sf::Vertex(pos);
                    skipNext = true;
                } else {
                    vertices[i - 1] = sf::Vertex(pos);
                }
            }
            vertices[i] = sf::Vertex(pos, sf::Color(180, 180, 180));
            if (skipNext) {
                i++;
            }
        }

        //Update 2D projections of TNB frame
        for (int i = 0; i < tnbVertices.getVertexCount(); i++) {
            sf::Vector2f pos;

            float t = (normal.z * screenHorizontal.z - normal.z * camera.z
                    + normal.y * screenHorizontal.y - normal.y * camera.y
                    + normal.x * screenHorizontal.x - normal.x * camera.x)
                    / (normal.x * (tnb[i].x - camPos.x)
                            + normal.y * (tnb[i].y - camPos.y)
                            + normal.z * (tnb[i].z - camPos.z));

            pos.x = camera.x + (tnb[i].x - camPos.x) * t;
            pos.y = camera.z + (tnb[i].z - camPos.z) * t;

            //Don't draw line if one of its vertices is offscreen
            bool skipNext = false;
            if (tnb[i].y < camPos.y) {
                pos.x = 0;
                pos.y = 0;

                if (i % 2 == 0) {
                    tnbVertices[i + 1] = sf::Vertex(pos);
                    skipNext = true;
                } else {
                    tnbVertices[i - 1] = sf::Vertex(pos);
                }
            }

            sf::Color color;
            if (i >= 0 && i < 2) {
                color = sf::Color::Red;
            } else if (i >= 2 && i < 4) {
                color = sf::Color::Green;
            } else {
                color = sf::Color::Cyan;
            }

            tnbVertices[i] = sf::Vertex(pos, color);
            if (skipNext) {
                i++;
            }
        }

        /////////// Update window display ////////////
        renderWindow.clear(sf::Color::Black);

        renderWindow.draw(vertices);
        renderWindow.draw(axisVertices);
        renderWindow.draw(tnbVertices);

        renderWindow.display();
    }
}

